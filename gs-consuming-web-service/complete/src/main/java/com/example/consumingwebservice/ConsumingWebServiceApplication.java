
package com.example.consumingwebservice;

import com.example.consumingwebservice.wsdl.Activity;
import com.example.consumingwebservice.wsdl.GetActivitiesResponse;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@SpringBootApplication
public class ConsumingWebServiceApplication extends Application {

    private static List<Activity> activities = new ArrayList<Activity>();

    public static void main(String[] args) {
        SpringApplication.run(ConsumingWebServiceApplication.class, args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(new Group());
        stage.setTitle("Activity History");
        stage.setWidth(500);
        stage.setHeight(500);

        List<String> activityNames = activities.stream().map(Activity::getActivityLabel).collect(Collectors.toList()).stream().distinct().collect(Collectors.toList());

        List<PieChart.Data> data = activityNames.stream().map(activityName -> {
			AtomicInteger hoursSum = new AtomicInteger();
        	activities.forEach(activity -> {
				if (activity.getActivityLabel().equals(activityName)) {
					hoursSum.addAndGet((getHoursDifference(activity)));
				}
			});
        	return new PieChart.Data(activityName, hoursSum.get());
		}).collect(Collectors.toList());

        ObservableList<PieChart.Data> observableData = FXCollections.observableList(data);

        final PieChart chart = new PieChart(observableData);
        chart.setTitle("Activities");

        ((Group) scene.getRoot()).getChildren().add(chart);
        stage.setScene(scene);
        stage.show();
    }

    @Bean
    CommandLineRunner lookup(ActivitiesClient quoteClient) {
        return args -> {
            GetActivitiesResponse response = quoteClient.getActivities();
            System.out.println("first activity: " + response.getActivities().get(0).getActivityLabel());

            activities.addAll(response.getActivities());

			launch();

            System.err.println(response.getActivities());
        };
    }

    public int getHoursDifference(Activity activity) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date start_time = null;
        Date end_time = null;

        try {
            start_time = formatter.parse(activity.getStartTime().toString().replace("T", " "));
            end_time = formatter.parse(activity.getEndTime().toString().replace("T", " "));
            long diff = end_time.getTime() - start_time.getTime();
            int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
            int diffhours = (int) (diff / (60 * 60 * 1000));
            int diffmin = (int) (diff / (60 * 1000));

            return diffhours;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

}
